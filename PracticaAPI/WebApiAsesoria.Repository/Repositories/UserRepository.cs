﻿using Microsoft.EntityFrameworkCore;
using Proyect2Parcial.Model.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApiAsesoria.Repository.Data;

namespace WebApiAsesoria.Repository.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll();
        User GetId(Guid id);
        void Insert(User user);
        void Delete(Guid id);
        void update(Guid id, User user);

    }
    public class UserRepository : IUserRepository
    {
        private readonly DataDbContext _dbContext = new DataDbContext();
        public void Delete(Guid id)
        {
 
            _dbContext.Remove(GetId(id));
            _dbContext.SaveChanges();


        }

        public User GetId(Guid id)
        {
            {
                return _dbContext.User.Find(id);
            }

        }

        public IEnumerable<User> GetAll()
        {

                return _dbContext.User;
        }

        public void Insert(User user)
        {
            _dbContext.User.Add(user);
            _dbContext.SaveChanges();

        }

        public void update(Guid id, User user)
        {
   
            var userNew =  GetId(id);
            userNew.Name = user.Name;
            userNew.NickName = user.NickName;
            userNew.LastName = user.LastName;
            userNew.PhoneNumber = user.PhoneNumber;
            _dbContext.SaveChanges();  
 
        }


    }
}
