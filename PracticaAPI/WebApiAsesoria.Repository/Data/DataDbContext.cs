﻿using Microsoft.EntityFrameworkCore;
using Proyect2Parcial.Model.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace WebApiAsesoria.Repository.Data
{
    public class DataDbContext : DbContext
    {
        public DataDbContext()
        {

        }

        public DataDbContext(DbContextOptions options)
        : base(options)
        {

        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //En caso de que el contexto no este configurado, lo configuramos mediante la cadena de conexion
            if (!optionsBuilder.IsConfigured)
            {
                _ = optionsBuilder.UseSqlServer("Server = PROGJR\\MSSQLSERVER01; Database = AdventureWorks; Integrated Security = true;");
            }
        }
        public virtual DbSet<User> User { get; set; }
    }
}
