﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Proyect2Parcial.Model.Model;
using Proyect2Parcial.Service.Service;


namespace PracticaAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService) {
            this._userService = userService;

        }
        // GET: api/User1
        [HttpGet]
        public IActionResult GetAllUser()
        {
            try
            {
                var Users = _userService.GetUsers();
                return Ok(Users);
            }
            catch (Exception)
            {

                return BadRequest("valio cabesa");
            }
        }

        // GET: api/User1/5
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            try
            {
                var userFinded = _userService.GetId(id);

                return Ok(userFinded);
            }
            catch (Exception)
            {

                throw;
            }
        }

        // POST: api/User1
        [HttpPost]
        public void PostUser([FromBody] User user)
        {
            try {
                if (ModelState.IsValid)
                {
                    _userService.AddUser(user);
                }


            }
            catch (Exception) 
            {

            }
            

        }

        // PUT: api/User1/5
        [HttpPut("{id}")]
        public IActionResult UpdateUser(Guid id, User user)
        {
  
            try
            {
                _userService.Update(id, user);
                return Ok("se actualizo el usuario sactisfactoriamente");
            }
            catch (Exception)
            {

                return BadRequest("valio cabe en la actualizacion prro");
            }

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {

            try
            {
                _userService.Delete(id);
  
            }
            catch (Exception)
            {

                BadRequest("valio cabesa men");
            }

        }
    }
}
