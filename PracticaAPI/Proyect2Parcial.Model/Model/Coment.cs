﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Proyect2Parcial.Model.Model
{
    class Coment : Entity
    {
        [Required]
        User idUser { get; set; }
        [Required]
        Documents idDocuments { get; set; }
        [Required, MaxLength(20),DataType(DataType.DateTime)]
        public DateTime date  { get; set; }
        [Required,MaxLength(500), DataType(DataType.Text)]
        public string ConteinerText { get; set; }

    }
}
