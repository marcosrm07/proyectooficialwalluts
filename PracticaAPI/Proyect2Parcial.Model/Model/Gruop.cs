﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Proyect2Parcial.Model.Model
{
    public class Gruop : Entity
    {
        [Required, MaxLength(50), DataType(DataType.Text), MinLength(3)]
        public string nameGroup { get; set; }
        [Required, MaxLength(50), DataType(DataType.Text), MinLength(3)]
        public DateTime dateCreation { get; set; }
        [Required, DataType(DataType.Date), MinLength(1)]
        public string numUser  { get; set; }
        [Required]

        List<Documents> documents;
    }
}
