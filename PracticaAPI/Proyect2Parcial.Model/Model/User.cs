﻿using Proyect2Parcial.Model.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Proyect2Parcial.Model.Model
{
    public class User: Entity
    {
        //[Required, MaxLength(200)]
        public string Name { get; set; }
        //[Required, MaxLength(200)]
        public string LastName { get; set; }
        //[Required, MaxLength(200)]
        public string NickName { get; set; }
        //[Required, MaxLength(200),DataType(DataType.Date)]
        public int YersOld { get; set; }
        //[Required, MaxLength(200)]
        public GenderEnum Gender { get; set; }
        //[Required, MaxLength(200)]
        public string PhoneNumber { get; set; }
        //[DataType(DataType.DateTime)]
        public DateTime Birthday { get; set; }
    }
}
