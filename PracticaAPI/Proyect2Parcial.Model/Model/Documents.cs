﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Proyect2Parcial.Model.Model
{
  public  class Documents : Entity
    {

        [Required, DataType(DataType.DateTime)]
        public DateTime DateUp { get; set; }
        [Required, DataType(DataType.DateTime)]
        public DateTime DateModification { get; set; }
        [Required, DataType(DataType.Text)]
        public string Type { get; set; }
        [Required, DataType(DataType.Date)]
        public int PermissionLevel { get; set; }
        [Required, DataType(DataType.Date)]
        public int Valuation { get; set; }

    }
}
