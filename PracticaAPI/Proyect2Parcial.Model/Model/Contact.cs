﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Proyect2Parcial.Model.Model
{
    class Contact : Entity
    {
        [Required]
        public List<Gruop> gruops;
        [Required]
        public List<Documents> documents;
        [Required, MaxLength(100),MinLength(3),DataType(DataType.Text)]
        public string nomGroup { get; set; }
        [Required,DataType(DataType.DateTime)]
        public DateTime dateCreation { get; set; }
        [Required,DataType(DataType.Date)]
        public  int NumUser { get; set; }

    }
}
