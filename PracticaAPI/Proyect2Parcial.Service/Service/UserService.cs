﻿using Proyect2Parcial.Model.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using WebApiAsesoria.Repository.Repositories;

namespace Proyect2Parcial.Service.Service
{
    public interface IUserService
    {
        IEnumerable<User> GetUsers();
        void Delete(Guid id);
        User GetId(Guid id);
        void AddUser(User user);
        void Update(Guid id, User user);
    }
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository) 
        {
            _userRepository = userRepository;
        }
        public IEnumerable<User> GetUsers()
        {
           
            return _userRepository.GetAll();
        }
        public void Delete(Guid id)
        {
            _userRepository.Delete(id);
        }

        public User GetId(Guid id)
        {
           return _userRepository.GetId(id);
        }

        public void AddUser(User user)
        {
            _userRepository.Insert(user);
        }



        public void Update(Guid id, User user)
        {
            _userRepository.update(id,user);
        }
    }
}
